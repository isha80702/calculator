
package calculator;

public interface CalculatorOperation {
    void perform();
}