
package calculator;

import java.security.InvalidParameterException;


public class Calculator {

    
    public static void main(String[] args) {
        
    }
    public void calculate(CalculatorOperation operation) {
        if (operation == null) {
            throw new InvalidParameterException("Cannot perform operation");
        }
        operation.perform();
    }
}

   
    
